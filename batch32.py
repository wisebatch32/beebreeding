hexa_coad= [[1, 0, -1], [0, 1, -1], [-1, 1, 0], [-1, 0, 1], [0, -1, 1], [1, -1, 0]]
RADIUS = 58
EDGES=6
def min_distance(hexaDict, hexa_num1, hexa_num2):
    p1, q1, r1 = hexaDict[hexa_num1]
    p2, q2, r2 = hexaDict[hexa_num2]

    return (abs(p2 - p1) + abs(q2 - q1) + abs(r2 - r1)) // 2

def createGrid(radius):
    hexa_num = 1
    hexaDict = {}
    p_coad, q_coad, r_coad = 0, 0, 0
    hexaDict[hexa_num] = p_coad, q_coad, r_coad
    for rad in range(radius):
        p_coad = 0
        q_coad = -rad
        r_coad = rad
        for i in range(6):
            for j in range(rad):
                p_coad += hexa_coad[i][0]
                q_coad += hexa_coad[i][1]
                r_coad += hexa_coad[i][2]
                hexa_num += 1
                hexaDict[hexa_num] = p_coad,q_coad, r_coad


    return hexaDict


for i in range(1,len(sys.argv),2):
    hexa_num1=int(sys.argv[i])
    hexa_num2=int(sys.argv[i+1])
    if hexa_num1 <= 0 and hexa_num2 <= 0:
        print("invalid cellnumber")
    else:
        print(min_distance(createGrid(RADIUS), hexa_num1, hexa_num2))
